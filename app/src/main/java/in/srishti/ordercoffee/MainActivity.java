package in.srishti.ordercoffee;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;

/**
 * This app displays an order form to order coffee.
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * This method is used to increment coffee quantity to be ordered.
     *
     * @param view
     */
    public void increment(View view) {
        int quantity = this.getQuantity();
        if (quantity == 100) {
            Toast.makeText(this, "You cannot order more than 100 coffees!", Toast.LENGTH_SHORT).show();
        } else {
            quantity += 1;
            setQuantity(quantity);
        }
    }

    /**
     * This method is used to decrement coffee quantity to be ordered.
     *
     * @param view
     */
    public void decrement(View view) {
        int quantity = this.getQuantity();
        if (quantity == 0) {
            Toast.makeText(this, "You cannot order less than 0 coffee!", Toast.LENGTH_SHORT).show();
        } else {
            quantity -= 1;
            setQuantity(quantity);
        }
    }

    /**
     * This method is called when the order button is clicked.
     *
     * @param view
     */
    public void submitOrder(View view) {
        int quantity = this.getQuantity();
        boolean whippedCream = this.hasWhippedCream();
        boolean chocolate = this.hasChocolate();
        float orderAmount = this.calculateAmount(whippedCream, chocolate, quantity);
        String name = this.getName();
        this.setOrderSummaryView(name, chocolate, whippedCream, quantity, orderAmount);
    }

    /**
     * Method to calculate order amount
     *
     * @param quantity: qty of coffees ordered by user
     * @return total amount calculated against number of coffees ordered
     */
    private float calculateAmount(boolean hasWhippedCream, boolean hasChocolate, int quantity) {
        float coffee_price = 5, cup_price = 2; // coffee_price is the price per coffee and cup_price is the price per paper cup
        float total_price = coffee_price + cup_price;
        float whippedCreamPrice = 3, chocolatePrice = 5;
        if (hasWhippedCream)
            total_price += whippedCreamPrice;
        if (hasChocolate)
            total_price += chocolatePrice;
        return (quantity * total_price);
    }

    /**
     * This method is called to fetch the name from the view
     *
     * @return name entered by user
     */
    private String getName() {
        EditText nameEditText = findViewById(R.id.name_editText);
        return nameEditText.getText().toString().trim();
    }

    /**
     * This method is called when the quantity is to be fetched from the view.
     *
     * @return currently displayed quantity in the view
     */
    private int getQuantity() {
        TextView quantityTextView = findViewById(R.id.coffee_qty_textview);
        return Integer.parseInt(quantityTextView.getText().toString());
    }

    /**
     * This method is called when quantity is to be displayed.
     *
     * @param quantity: number of coffees ordered
     */
    private void setQuantity(int quantity) {
        TextView quantityTextView = findViewById(R.id.coffee_qty_textview);
        quantityTextView.setText(Integer.toString(quantity));
    }

    /**
     * This method is called when the state of the whipped cream topping is to be fetched from the view.
     *
     * @return TRUE if the whipped cream checkbox is checked; FALSE otherwise
     */
    private boolean hasWhippedCream() {
        CheckBox whippedCreamCheckBox = findViewById(R.id.whippedCream_checkbox);
        return whippedCreamCheckBox.isChecked();
    }

    /**
     * This method is called when the state of the chocolate topping is to be fetched from the view.
     *
     * @return TRUE if the chocolate checkbox is checked; FALSE otherwise
     */
    private boolean hasChocolate() {
        CheckBox chocolateCheckBox = findViewById(R.id.chocolate_checkbox);
        return chocolateCheckBox.isChecked();
    }

    /**
     * This method is called when price is to be displayed.
     *
     * @param quantity: number of coffees ordered
     * @param amount:   total amount to be paid
     */
    private void setOrderSummaryView(String name, boolean hasWhippedCream, boolean hasChocolate, int quantity, float amount) {
        String message;
        if (quantity > 0) {
            message = (name != null ? getString(R.string.order_summary_name, name) : "") +
                    "\n" + getString(R.string.order_summary_quantity, quantity) +
                    "\n" + (hasWhippedCream ? getString(R.string.order_summary_whippedCream, getString(R.string.yes)) : getString(R.string.order_summary_whippedCream, getString(R.string.no))) +
                    "\n" + (hasChocolate ? getString(R.string.order_summary_chocolate, getString(R.string.yes)) : getString(R.string.order_summary_chocolate, getString(R.string.no))) +
                    "\n" + getString(R.string.order_summary_amount, NumberFormat.getCurrencyInstance().format(amount)) +
                    "\n" + getString(R.string.thank_you);
            this.sendEmail(message);
        } else
            Toast.makeText(this, "Your order is empty! Please add atleast one item!", Toast.LENGTH_SHORT).show();
    }

    /**
     * This method is called when the order summary is to be sent as an email to the user
     *
     * @param bodyText: text string to be sent in body of the email
     */
    private void sendEmail(String bodyText) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // this ensures that only email apps will handle the intent
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.order_coffee_summary));
        intent.putExtra(Intent.EXTRA_TEXT, bodyText);
        if (intent.resolveActivity(getPackageManager()) != null)
            startActivity(Intent.createChooser(intent, "Send mail..."));
        else
            Toast.makeText(this, "There is no email client installed on your device!", Toast.LENGTH_SHORT).show();
    }
}